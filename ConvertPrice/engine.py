# -*- coding: utf-8 -*-
import pandas as pd
import csv
import datetime
from io import StringIO


class DoubleCodeError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return ', '.join(self.value)

def input_path_to_output_path(file_path):
    reversed_string = file_path[::-1]
    dot_index = reversed_string.index('.') + 1
    without_extension = (reversed_string[dot_index::])[::-1]
    dte = str(datetime.datetime.now())[:10]
    dtt = str(datetime.datetime.now())[11:19]
    output_path = '{}_{}.xlsx'.format(without_extension, dte)
    return output_path


def csv_to_list(csv_file):
    list_to_return = []
    csv_file.seek(0)
    string_reader = csv.reader(csv_file, delimiter=';')
    for row in string_reader:
        list_to_return.append(row)
    return list_to_return


def convert_list_for_upload(input_list: list = [], pricelist_code='',
                            target_code: str = '', round_needed: bool = True,
                            contains_cito: bool = False, b2b_price: bool = False):
    code_index = None
    while code_index is None:
        for row in input_list:
            try:
                code_index = row.index(target_code)
            except ValueError:
                pass
    list_to_return = [5 * ['', ], [pricelist_code, '', '', '', 'z']] if b2b_price else [4 * ['', ], ['', '', '', 'z']]
    codes = []
    for row2 in input_list:
        code = row2[code_index]
        last = row2[-1]
        prelast = row2[-2]
        if len(code) == 4:
            if contains_cito:
                if round_needed:
                    try:
                        price = int(round(float(prelast), 0))
                        cito_price = int(round(float(last), 0))
                    except ValueError:
                        price = prelast
                        cito_price = last
                else:
                    price = prelast
                    cito_price = last
                if code == 'B050' or code == 'B049' or code == 'B035':
                    cito = 1
                    cito_price = price
                elif cito_price != '':
                    try:
                        int(cito_price)
                        cito = 1
                    except ValueError:
                        cito_price = ''
                        cito = '0'
                else:
                    cito_price = ''
                    cito = '0'
            else:
                if round_needed:
                    try:
                        price = int(round(float(last), 0))
                    except ValueError:
                        price = last
                else:
                    price = last
                if code == 'B050' or code == 'B049' or code == 'B035':
                    cito_price = price
                    cito = 1
                else:
                    cito_price = ''
                    cito = '0'
            temp_list = [code, price, cito, cito_price] if b2b_price else [code, price, cito_price]
            list_to_return.append(temp_list)
            codes.append(code)
    uniq_codes = set([x for x in codes if codes.count(x) > 1])
    if len(uniq_codes) > 0:
        raise DoubleCodeError(uniq_codes)
    return list_to_return


def list_to_csv(input_list):
    buffer = StringIO()
    buffer.seek(0)
    writer = csv.writer(buffer, delimiter=';', quotechar='|', quoting=csv.QUOTE_ALL)
    writer.writerows(input_list)
    return buffer


def reformat_excel(input_excel_file, output_excel_file,
                   pricelist_code, target_code,
                   round_needed: bool = True, contains_cito: bool = False,
                   b2b_price: bool = False):
    csv1 = StringIO()
    data_xlsx = pd.read_excel(input_excel_file, sheet_name=0, index_col=None)
    data_xlsx.to_csv(csv1, encoding='utf-8', sep=';', index=False, header=False)
    price = csv_to_list(csv1)
    idx = convert_list_for_upload(input_list=price, pricelist_code=pricelist_code,
                                  target_code=target_code, round_needed=round_needed,
                                  contains_cito=contains_cito, b2b_price=b2b_price)
    csv2 = list_to_csv(idx)
    csv2.seek(0)
    data_csv = pd.read_csv(csv2, delimiter=';', encoding='utf-8',
                           index_col=None, quoting=csv.QUOTE_MINIMAL, quotechar='|', )
    data_csv.to_excel(output_excel_file, encoding='cp1251', index=False, merge_cells=False, header=False)
