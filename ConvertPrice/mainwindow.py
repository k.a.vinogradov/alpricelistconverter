# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './mainwindow.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
import sys


class Ui_PricelistConverter(object):
    def setupUi(self, PricelistConverter):
        PricelistConverter.setObjectName("PricelistConverter")
        PricelistConverter.setGeometry(QtCore.QRect(800, 400, 600, 300))
        PricelistConverter.setFixedSize(282, 417)
        PricelistConverter.setWindowIcon(QtGui.QIcon('resources/icon_48.png'))
        self.centralwidget = QtWidgets.QWidget(PricelistConverter)
        self.centralwidget.setObjectName("centralwidget")
        self.toB2BButton = QtWidgets.QPushButton(self.centralwidget)
        self.toB2BButton.setGeometry(QtCore.QRect(20, 350, 94, 34))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("resources/b2b.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.toB2BButton.setIcon(icon)
        self.toB2BButton.setObjectName("toB2BButton")
        self.toB2CButton = QtWidgets.QPushButton(self.centralwidget)
        self.toB2CButton.setGeometry(QtCore.QRect(160, 350, 94, 34))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("resources/b2c.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.toB2CButton.setIcon(icon)
        self.toB2CButton.setObjectName("toB2CButton")
        self.priceCode = QtWidgets.QTextEdit(self.centralwidget)
        self.priceCode.setGeometry(QtCore.QRect(20, 310, 91, 31))
        self.priceCode.setObjectName("priceCode")
        self.targetCode = QtWidgets.QTextEdit(self.centralwidget)
        self.targetCode.setGeometry(QtCore.QRect(20, 240, 91, 31))
        self.targetCode.setObjectName("targetCode")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(20, 280, 81, 20))
        self.label.setObjectName("label")
        self.label2 = QtWidgets.QLabel(self.centralwidget)
        self.label2.setGeometry(QtCore.QRect(20, 210, 101, 31))
        self.label2.setObjectName("label2")
        self.openFilePath = QtWidgets.QTextEdit(self.centralwidget)
        self.openFilePath.setGeometry(QtCore.QRect(20, 50, 241, 51))
        self.openFilePath.setObjectName("openFilePath")
        self.openButton = QtWidgets.QPushButton(self.centralwidget)
        self.openButton.setGeometry(QtCore.QRect(20, 10, 121, 34))
        self.openButton.setObjectName("openButton")
        self.saveButton = QtWidgets.QPushButton(self.centralwidget)
        self.saveButton.setGeometry(QtCore.QRect(20, 110, 121, 34))
        self.saveButton.setObjectName("saveButton")
        self.saveFilePath = QtWidgets.QTextEdit(self.centralwidget)
        self.saveFilePath.setGeometry(QtCore.QRect(20, 150, 241, 51))
        self.saveFilePath.setObjectName("saveFilePath")
        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setGeometry(QtCore.QRect(130, 280, 20, 81))
        self.line.setFrameShape(QtWidgets.QFrame.VLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.RoundPrices = QtWidgets.QCheckBox(self.centralwidget)
        self.RoundPrices.setGeometry(QtCore.QRect(130, 210, 131, 21))
        self.RoundPrices.setObjectName("RoundPrices")
        self.containsCito = QtWidgets.QCheckBox(self.centralwidget)
        self.containsCito.setGeometry(QtCore.QRect(130, 240, 131, 31))
        self.containsCito.setObjectName("containsCito")
        PricelistConverter.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(PricelistConverter)
        self.statusbar.setObjectName("statusbar")
        PricelistConverter.setStatusBar(self.statusbar)

        self.retranslateUi(PricelistConverter)
        QtCore.QMetaObject.connectSlotsByName(PricelistConverter)

    def retranslateUi(self, PricelistConverter):
        _translate = QtCore.QCoreApplication.translate
        PricelistConverter.setWindowTitle("PriceConverter")
        self.toB2BButton.setText("В B2B")
        self.toB2CButton.setText("В B2C")
        self.label.setText("Код прайса")
        self.targetCode.setText('K001')
        self.label2.setText("Найти колонку\nпо коду")
        self.openButton.setText("Открыть файл")
        self.saveButton.setText("Сохранить как")
        self.RoundPrices.setText("Округлить цены")
        self.containsCito.setText(_translate('PricelistConverter', 'Прайс содержит\nцены Cito'))
        self.RoundPrices.setChecked(True)
        self.containsCito.setChecked(False)


class Ui_DialogOK(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.setGeometry(800, 400, 272, 79)
        self.messageText = QtWidgets.QLabel(Dialog)
        self.messageText.setObjectName("label")
        self.pushButtonOK = QtWidgets.QPushButton(Dialog)
        self.pushButtonOK.setFixedSize(86, 28)
        self.pushButtonOK.setObjectName("pushButtonOK")

        Dialog.hbox = QtWidgets.QHBoxLayout()
        Dialog.hbox.addStretch(1)
        Dialog.hbox.addWidget(self.pushButtonOK)

        Dialog.vbox = QtWidgets.QVBoxLayout()
        Dialog.vbox.addWidget(self.messageText)
        Dialog.vbox.addStretch(1)
        Dialog.vbox.addLayout(self.hbox)

        Dialog.setLayout(Dialog.vbox)


        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle("Ошибка")
        self.messageText.setText("Что-то пошло не так")
        self.pushButtonOK.setText("OK")


class One_button_dialog(QtWidgets.QDialog, Ui_DialogOK):
    def __init__(self):
        QtWidgets.QDialog.__init__(self)
        self.setupUi(self)
        self.pushButtonOK.clicked.connect(self.close)
