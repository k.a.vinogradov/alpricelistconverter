# -*- coding: utf-8 -*-

import xlrd
from mainwindow import *
from PyQt5 import QtWidgets
from engine import input_path_to_output_path, reformat_excel, DoubleCodeError
import os.path


class MyWin(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        QtWidgets.QMainWindow.__init__(self, parent)
        self.ui = Ui_PricelistConverter()
        self.ui.setupUi(self)

        self.ui.openButton.clicked.connect(self.openFileNameDialog)
        self.ui.saveButton.clicked.connect(self.saveFileDialog)
        self.ui.toB2BButton.clicked.connect(self.b2b_price)
        self.ui.toB2CButton.clicked.connect(self.b2c_price)

    def openFileNameDialog(self):
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Открыть файл", "",
                                                            "Excel File (*.xls, *.xlsx);;All Files (*)")
        if fileName:
            self.ui.openFilePath.setText(fileName)
            self.ui.saveFilePath.setText(input_path_to_output_path(fileName))

    def saveFileDialog(self):
        fileName, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Сохранить как", "",
                                                            "Excel File (*.xlsx);;All Files (*)")
        if fileName:
            file_name_with_extens = '{}.xlsx'.format(fileName)
            self.ui.saveFilePath.setText(file_name_with_extens)

    def pricelist_converter(self, b2b_price: bool = False):
        excel_to_open = self.ui.openFilePath.toPlainText()
        excel_to_save = self.ui.saveFilePath.toPlainText()
        pricelist_code = self.ui.priceCode.toPlainText() if b2b_price else ''
        target_code = self.ui.targetCode.toPlainText()
        round_needed = self.ui.RoundPrices.checkState()
        contains_cito = self.ui.containsCito.checkState()
        if excel_to_open == excel_to_save != '':
            self.okDialog(message_text='Нельзя перезаписать исходный файл')
            return
        elif excel_to_open == '':
            self.okDialog(message_text='Не указан исходный файл')
            return
        elif not os.path.isfile(excel_to_open):
            self.okDialog(message_text='Файл не существует')
            return
        elif excel_to_save == '':
            self.okDialog(message_text='Не указан конечный файл')
            return
        elif os.path.isfile(excel_to_save):
            self.okDialog(message_text='Файл уже существует')
            return
        elif target_code == '':
            self.okDialog(message_text='Не указан Код исследования')
            return
        if b2b_price and pricelist_code == '':
            self.okDialog(message_text='Не указан Код прайса')
            return
        else:
            try:
                reformat_excel(input_excel_file=excel_to_open,
                               output_excel_file=excel_to_save,
                               pricelist_code=pricelist_code,
                               target_code=target_code,
                               round_needed=round_needed,
                               contains_cito=contains_cito,
                               b2b_price=b2b_price)
                self.ui.openFilePath.setText('')
                self.ui.saveFilePath.setText('')
                self.ui.priceCode.setText('')
                self.okDialog(window_title='Конвертация завершена', message_text='Конвертация успешно завершена')
            except UnboundLocalError:
                self.okDialog(message_text='Код исследования отсутствует в файле')
            except xlrd.biffh.XLRDError:
                self.okDialog(message_text='Некорректный формат файла')
            except DoubleCodeError as dce:
                self.okDialog(message_text='Коды дублируются: {}'.format(dce))

    def b2b_price(self):
        self.pricelist_converter(b2b_price=True)

    def b2c_price(self):
        self.pricelist_converter(b2b_price=False)

    def okDialog(self, window_title='Ошибка', message_text=''):
        d = One_button_dialog()
        d.setWindowTitle(window_title)
        d.messageText.setText(message_text)
        d.exec_()



if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    myapp = MyWin()
    myapp.show()
    sys.exit(app.exec_())
