from setuptools import setup

setup(
    name='ConvertPriceLQ',
    version='1.0',
    packages=['ConvertPrice'],
    install_requires=[
          'pandas', 'xlrd', 'PyQt5', 'openpyxl'
      ],
    package_dir={'': 'ConvertPrice'},
    url='',
    license='CC',
    author='kvinogradov',
    author_email='k.a.vinogradov@gmail.com',
    description='converts excel file into file suitable to upload to LIS'
)
